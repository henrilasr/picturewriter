import exif

import PIL.Image
import PIL.ImageDraw
import PIL.ImageFont

from tkinter import filedialog
from tkinter import messagebox
from tkinter import *
from tkinter import ttk

from os import listdir


##ajout de la date sur l'image
def addDate(imgName):
    def getDate(img):
        with open(img, 'rb') as image_file:
            try:
                my_image = exif.Image(image_file)
                date = my_image.datetime_original[0:10]
                date = date[-2:]+'/'+date[5:7]+'/'+date[0:4]
            except:
                return imgName
        return(date)

    def writeDate(imgName):
        date = getDate(imgName)
        if date!=imgName:
            image = PIL.Image.open(imgName)
            imgSize = image.size
            nbPxl = image.size[0]*image.size[1]
            tailleTxt = max([20,int(imgSize[1]/12)])
            fnt = PIL.ImageFont.truetype('arial.ttf', tailleTxt)
            d = PIL.ImageDraw.Draw(image)
            d.text((10,10), date, font=fnt, fill=(255,0,0))
            image.save(imgName,exif=image.info['exif'])
            return 0
        else:
            return imgName

    return writeDate(imgName)

#ajout du fichier sélectionné à la liste
def selectFile():
    global info
    filenames =  filedialog.askopenfilenames(title = "Choisir une image",filetypes = (("Images","*.jpg;*.jpeg;*.png"),("all files","*.*")))
    if filenames != "":
        for filename in filenames:
            if info.cget("text") == info_init:
                info.config(text = filename[filename.rfind('/')+1:])
            else:
                info.config(text = info.cget("text")+"\n"+filename[filename.rfind('/')+1:])
            if filename[filename.rfind('.')+1:].lower() in ['jpg', 'jpeg', 'png']:
                    pics.append(filename)
#ajout des fichiers du dossier sélectionné à la liste
def selectFolder():
    global info
    filename =  filedialog.askdirectory(title = "Choisir un dossier")
    if filename != "":
        if info.cget("text") == info_init:
            info.config(text = filename[filename.rfind('/')+1:])
        else:
            info.config(text = info.cget("text")+"\n"+filename[filename.rfind('/')+1:])
        for elt in listdir(filename):
            if elt[elt.rfind('.')+1:].lower() in ['jpg', 'jpeg', 'png']:
                elt = filename+"/"+elt
                pics.append(elt)

#suppression du dernier de la liste
def removeLast():
    global info
    l = info.cget("text")
    if "\n" in l:
        last = l[l.rfind("\n")+1:]
        aGarder = l[:l.rfind("\n")]
    elif l!=info_init:
        last = l
        aGarder = info_init
    else :return
    info.config(text=aGarder)
    for pic in pics:
        if pic[pic.rfind('/')+1:] == last:
            pics.remove(pic)

#lancement de l'ajout de la date sur les photos sélectionnées
def startProcess():
    global pics
    p2 = []
    for e in pics:
        if e not in p2:
            p2.append(e)
    pics = p2
    total = len(pics)
    errors = []
    bar.config(maximum=total)
    for pic in pics:
        s = addDate(pic)
        if s != 0:
            errors.append(s)
        bar.config(value=bar.cget("value")+1)
        bar.update()
    pics = []
    info.config(text=info_init)
    if errors == []:
        messagebox.showinfo("Fin de traitement", message="Toutes les images ont été traitées")
    else:
        if len(errors) == 1:
            msg = errors[0]+ " n'a pas pu être traitée."
        else:
            msg = "Les images suivantes n'ont pas pu être traitées :\n"
            for img in errors:
                msg += "  -  "+img+"\n"
        messagebox.showerror("Fin de traitement", message=msg)
    bar.config(value=0)
    bar.update()

#Création de l'interface graphique

pics = []

root = Tk()
root.title("Dater les images")
info_init = "Aucun fichier sélectionné"
info = Label(root, text=info_init)
BtSelectFile = Button(root,text="Choisir une image", command=selectFile)
BtSelectFolder = Button(root,text="Choisir un dossier", command=selectFolder)
BtGo = Button(root, text="Valider", command=startProcess)
BtCancel = Button(root,text="Retirer", command=removeLast)
titre = Label(root, text="Choisir le(s) fichier(s) :")
bar = ttk.Progressbar(root,orient=HORIZONTAL,mode="determinate",length=300)

titre.grid(row=0,column=0,columnspan=2)
BtSelectFile.grid(row=1,column=0)
BtSelectFolder.grid(row=1,column=1)
info.grid(row=2,column=0,columnspan=2)
BtCancel.grid(row=3, column=0)
BtGo.grid(row=3,column=1)
bar.grid(row=4,column=0,columnspan=2,padx=20,pady=20)
root.mainloop()